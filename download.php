<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		首页，通过参数显示相关信息
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 加载所需配置文件
require_once('./vx_download/config.php');

// 获取传递的url参数
$url=$_GET['url'];

// 执行查询命令
$sql_0 = $conn->query("select * from vx_download_share where url= '$url' ");
$arr = mysqli_fetch_array($sql_0,MYSQLI_ASSOC);

// 获取文章标题 
$title = $arr["title"];
// 获取阿里分享标题
$ali_title = $arr["ali_title"];
// 获取阿里分享链接
$ali_share = $arr["ali_share"];
// 获取阿里分享密码
$code = $arr["code"];
// 获取其他分享标题
$other_title = $arr["other_title"];
// 获取其他分享链接
$other_share = $arr["other_share"];
// 整合文章链接
$com_url = BLOG_URL."/post-".$url.".html";
// 获取当前访客量
$uv_visitor = $arr['uv_visitor'];

// 如果没有数据报错
if(empty($title)){
	die("<div style='width:auto;height:auto;margin: 12% auto;text-align: center;'><span style='color:red'>非法操作！当前数据不存在！</span></br></br>请联系管理员处理！<br/> ".USER_MAIL."</div>");
}

// 在有数据的情况下，浏览量 + 1
$uv_visitor = $uv_visitor + 1;
$sql_uv= "update vx_download_share set uv_visitor='{$uv_visitor}' WHERE url='{$url}'";
if(!($conn->query($sql_uv)))
{
   die("<div style='width:auto;height:auto;margin: 12% auto;text-align: center;'><span style='color:red'>非法操作！当前数据不存在！</span></br></br>请联系管理员处理！<br/> ".USER_MAIL."</div>");
}

?>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--告诉爬虫该页面上所有链接都无需追踪。-->
<meta name="robots" content="nofollow" /> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?echo BLOG_NAME?>资源下载页面-vxia.net</title>
<meta name="keywords" content="微夏博客网 -- vxia.net"/>
<meta name="description" content="微夏博客网资源下载页面"/>
<style>
*{margin:0;padding:0;}
a{color:#ff3800;text-decoration:none;}
a:hover{color:green;border-bottom:1px dashed #F30;}
a img{border:0}
ul{margin:5px;padding:5px;}
ul li{line-height:15px;font-size:14px;padding:5px;border-bottom:1px dashed #ddd;list-style:none outside none;}
h1{text-align:center;border-bottom:1px solid #dadada;clear:both;font:26px Georgia,"Times New Roman",Times,serif;margin:5px 0 0 -4px;padding:0;padding-bottom:7px;font-weight:bold;}
h2{color:#FF0080;font-size:16px;}
h3{color:#666666;font-size:15px;margin:15px 0 10px;}
p{padding-bottom:2px;font-size:13px;line-height:22px;}
html{background:#F2F2F2;}
body{background:#fff;color:#333;font-family:"Microsoft YaHei",Helvetica,sans-serif;margin:2em auto;width:728px;padding:1em 2em;border:1px solid #dfdfdf;}
.clear{clear:both;height:1px;}
#header{padding:10px 0 15px 10px;text-align:center;}
.list{background:none repeat scroll 0 0 #F2F2F2;border:1px solid #BFBFBF;color:#666666;font-size:14px;margin:15px 0 10px;padding:10px 0 0 10px;}
.list a{display:inline-block;padding:2px 4px;}
.sm,.desc{background:#F2F2F2;border:1px solid #BFBFBF;color:#666666;font-size:13px;margin:10px 0;}
</style>
</head>
<body>
<div id="header"></div>
<h1><a href="<? echo $com_url ?>"target="_blank"><? echo $title;?></a></h1>
<div class="clear"></div>
<div class="desc" style="padding:10px;">
	<h3>微夏博客网下载文件资源信息</h3>
	<p>文件名称：<? echo $title;?></p>
	<p></p>
</div>
<div class="clear">
</div>

<div class="list">
	下载列表：<br/>
	<? echo $ali_title;?>：<a href="<? echo $ali_share;?>"target="_blank">
	<? echo $ali_share;?></a>&nbsp;&nbsp;提取码：<? echo $code;?>
	<br>
	<? echo $other_title;?>：<a href="<? echo $other_share;?>"target="_blank">
	<? echo $other_share;?></a>&nbsp;&nbsp;<br>
	</div>
<div class="clear"></div>
<div class="desc" style="border:none"></div>
<div class="clear"></div>

<div class="desc" style="border:1px solid #FCC;background:#FFE;">
	<p>下载说明</p>
	<ol style="padding:0 10px 0 25px;">
		<li>下载后文件若为压缩包格式，请安装RAR或者好压软件进行解压。</li>
		<li>文件比较大的时候，建议使用下载工具进行下载，浏览器下载有时候会自动中断，导致下载错误</li>
		<li>资源可能会由于内容问题被和谐，导致下载链接不可用，遇到此问题，请到文章页面进行反馈，我们会及时进行更新的。</li>
		<li>其他下载问题请自行搜索教程，这里不一一讲解</li>
	</ol>
</div>

<div class="sm" style="padding:5px">
	<p>声明：</p>
	<p>本站大部分下载资源收集于网络，只做学习和交流使用，版权归原作者所有，若为付费资源，请在下载后24小时之内自觉删除，若作商业用途，请到原网站购买，由于未及时购买和付费发生的侵权行为，与本站无关。本站发布的内容若侵犯到您的权益，请联系本站删除，我们将及时处理！
	</p>
</div>
<div class="clear"></div>
<div class="copy" style="text-align: center;margin-top:15px;">
	Copyright &copy; 微夏博客网 vxia.net
</div>
</br>
<div style="text-align: center;font-size: 13px;">当前页面已被访问<span style="color:red"> <?echo $uv_visitor?> </span>次</div>

</body>
</html>


