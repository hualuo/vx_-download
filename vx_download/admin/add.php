<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		添加相关数据
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 加载所需配置文件
require_once("../config.php");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 开启session
session_start();

//  判断是否登陆
if (empty($_SESSION['username'])){
	header('location:./login.php');
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--引用文件-->
<link rel="stylesheet" href="./style/layui.css" media="all">
<link rel="stylesheet" href="./style/auto.css" media="all">
<script src="./style/layui.js"></script>

<!-- 添加标题 -->
<title><? echo BLOG_NAME ?> - 添加数据</title>
</head>
<body>
<br/><br/>
<center><h1>添加数据</h1></center>

<!-- 表单数据列表 -->
<div id="add_box">
<form action="add_share.php" class="layui-form layui-form-pane"class="layui-form layui-form-pane" style="width: 100%;" method="post">
	<div class="layui-form-item">
	<label class="layui-form-label">文章序号</label>
	<div class="layui-input-block"  style="width: 50%">
	<input type="text" name="url" onblur="value=vx_num(this.value)" autocomplete="off" placeholder="请输入文章序号" class="layui-input">
	</div><span id="notice">非整数自动转为 0 <span>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label">文章标题</label>
	<div class="layui-input-block">
	<input type="text" name="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label"><input class="layui-input" style="border: 1px solid;height: 25px;" name="ali_title" value="阿里云盘" id="input_title_box" autocomplete="off"></label>
	<div class="layui-input-block">
	<input type="text" name="ali_share" autocomplete="off" placeholder="请输入阿里云盘分享链接" class="layui-input">
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label">提取密码</label>
	<div class="layui-input-block">
	<input type="text" name="code" value="暂无" autocomplete="off" placeholder="请输入阿里云盘提取码" class="layui-input">
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label"><input class="layui-input" style="border: 1px solid;height: 25px;" name="other_title" value="其他云盘" id="input_title_box" autocomplete="off"></label>
	<div class="layui-input-block">
	<input type="text" name="other_share" autocomplete="off" placeholder="请输入其他云盘分享链接" class="layui-input">
	</div>
	</div>
	<br/><br/><br/>
	<div class="layui-input-block">
      <button type="submit" id="add_button" class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
    </div>
</form>
</div>

<script>
// 去除所有非整数字符 
function vx_num(value) {
   value = value.replace(/[^\d]/g, '').replace(/^0{1,}/g, '');
   if (value != '')
      value = parseFloat(value).toFixed(0);
   else
      value = parseFloat(0).toFixed(0);
   return value;
}
</script>
</body>
</html>