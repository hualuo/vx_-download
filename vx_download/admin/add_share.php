<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		后台执行添加数据
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 加载文件
require_once("../config.php");

// 启动 Session
session_start();

// 判断是否登陆
if (empty($_SESSION['username'])){
	header('location:./login.php');
}

// 获取并处理接收到的信息
$url =  htmlspecialchars($_POST["url"]);
$title =  htmlspecialchars($_POST["title"]);
$ali_title =  htmlspecialchars($_POST["ali_title"]);
$ali_share =  htmlspecialchars($_POST["ali_share"]);
$code =  htmlspecialchars($_POST["code"]);
$other_title =  htmlspecialchars($_POST["other_title"]);
$other_share =  htmlspecialchars($_POST["other_share"]);
$uv_visitor = 0;

// 执行插入数据库
$sql="insert into vx_download_share(url,title,ali_title,ali_share,code,other_title,other_share,uv_visitor) values ('{$url}','{$title}','{$ali_title}','{$ali_share}','{$code}','{$other_title}','{$other_share}','{$uv_visitor}')";



// 判断数据库是否执行成功 
if($conn->query($sql))
{
  header("location:index.php");
}
else{
  echo "非法字符！修改失败！";
}


?>