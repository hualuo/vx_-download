<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		检测账号密码是否正确
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 开启session
session_start();

// 链接数据库
require_once('../config.php');

// 判断是否输入用户名和密码
if(empty($_POST['username'])){
	echo "<script>alert(' 非法操作! 已记录 IP 地址！');window.location.href='./login.php';</script>";
}
if(empty($_POST['password'])){
	echo "<script>alert(' 请输入密码!');window.location.href='./login.php';</script>";
}

// 处理接收的信息并查询数据库
$username = htmlspecialchars($_POST['username']);
$password = htmlspecialchars($_POST['password']);
$refer = mysqli_query($conn,"select * from vx_download_user where username = '$username'");

// 数组形式处理结果
$dat = mysqli_fetch_assoc($refer);


// 判断用户名和密码是否正确
if($dat){
	if($dat['password'] == $password){
		$_SESSION['username'] = $username;
		echo "<script>window.location.href='./index.php';</script>";
	}else{
		// 密码错误
		echo "<script>alert('用户名或密码错误，登录失败！');window.location.href='./login.php';</script>";
	}

}else{
		// 找不到此用户
		echo "<script>alert('用户名或密码错误，登录失败！');window.location.href='./login.php';</script>";
	}









?>

