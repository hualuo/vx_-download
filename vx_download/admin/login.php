<?

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		登录信息输入界面
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 加载所需配置文件
require_once("../config.php");
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><? echo BLOG_NAME ?> - 管理登录</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="./style/main.css">
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form-title" style="background-image: url(./style/bg-01.jpg);">
                    <span class="login100-form-title-1"><? echo BLOG_NAME ?>管理登录</span>
                </div>
                <form class="login100-form validate-form" action="login-check.php" method="post">
                    <div class="wrap-input100 validate-input m-b-26" data-validate="用户名不能为空">
                        <span class="label-input100">用户名</span>
                        <input class="input100" type="text" name="username" placeholder="请输入用户名">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-18" data-validate="密码不能为空">
                        <span class="label-input100">密码</span>
                        <input class="input100" type="password" name="password" placeholder="请输入用户名">
                        <span class="focus-input100"></span>
                    </div>

                   
					
                    <div class="container-login100-form-btn" style="position: absolute;top: 80%;left: 37%;">
                        <button class="login100-form-btn" type="submit" name="submit">登 录</button>
                    </div>
					
                </form>
            </div>
        </div>
    </div>


</body>
</html>