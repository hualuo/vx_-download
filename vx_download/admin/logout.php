<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		退出登录
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

session_start();
// 这种方法是将原来注册的某个变量销毁
unset($_SESSION['username']);
// 这种方法是销毁整个 Session 文件
session_destroy();

//跳回原界面
header('Location:./index.php')

?>