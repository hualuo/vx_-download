<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		搜索功能
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 链接数据库
require_once("../config.php");

// 启动 Session
session_start();

// 判断是否登陆
if (empty($_SESSION['username'])){
	header('location:./login.php');
}

// 如果没有页数参数，则设置为第1页
$page=isset($_GET['p'])? $_GET['p']:1;

// 获取要搜索的标题名称
$title = $_GET["title"];

// 查询语句，分页查询每页10条信息
// limit后的两个参数第一个是查询的起始位置，第二个是显示的数据条数
$sql = "select * from vx_download_share where title LIKE '%$title%' limit ".($page-1) * 10 .",10 ";
$result=mysqli_query($conn,$sql);

// 模糊查询获取总页数
$sql_0 = "select * from vx_download_share where title LIKE '%$title%' ";
$result_0=mysqli_query($conn,$sql_0);
$rows_0 = mysqli_num_rows($result_0);

// 获取总页数（总行数/10）
$count_page=ceil($rows_0/10);



?>
<html>
<head>
	<!-- 加载样式文件 -->
	<link rel="stylesheet" href="./style/layui.css" media="all">
	<link rel="stylesheet" href="./style/auto.css" media="all">
	<script src="./style/layui.js"></script>
</head>

<h3 id="title"><a href="index.php" ><?echo BLOG_NAME?> - 管理界面</a></h3>

<table class="layui-table">
<div class="layui-btn-container"> 
	<a href="add.php" target="_blank"><button class="layui-btn layui-btn-sm"  lay-event="add">新增数据</button> </a>
	<form type="text" method="get" action="search.php">
		<input class="layui-input" name="title" id="input_box" autocomplete="off">
		<button class="layui-btn layui-btn-md layui-btn-normal" id="in_button" lay-submit lay-filter="queryUser" type="submit">标题查询</button>
	<form>
</div>
<colgroup>
	<!-- 设置表格宽度 -->
    <col width="80">
    <col width="550">
	<col width="300">
	<col width="80">
	<col width="160">
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>链接</th>
      <th>文章标题</th>
      <th>阿里分享</th>
	  <th>密码</th>
	  <th>其他分享</th>
	  <th>操作</th>
    </tr> 
  </thead>
  <tbody>

<?php
	$num = 1;
	while ($rows=mysqli_fetch_assoc($result)){
		echo "<tr>";
		echo "<td>{$rows['url']}</td>";
		echo "<td>{$rows['title']}</td>";
		echo "<td>{$rows['ali_share']}</td>";
		echo "<td id='code'>{$rows['code']}</td>";
		echo "<td>{$rows['other_share']}</td>";
		// 获取序号值
		echo "<script>var url_number".$num."=".$rows['url'].";</script>";
		// 操作管理
		echo "<td id='td'> 
			<a class='layui-btn layui-btn-xs'  href='javascript:copy".$num."()' >复制</a> &nbsp;&nbsp;
			<a class='layui-btn layui-btn-danger layui-btn-xs' href='javascript:del({$rows['url']})'>删除</a>&nbsp;&nbsp;
			<a class='layui-btn layui-btn-xs' href='edit.php?url={$rows['url']}'  >修改</a>
			</td> ";
		echo "</tr>";
		// 复制功能
		echo "<script type='text/javascript'>
			function copy".$num."(){
				// 生成下载链接
					url_link".$num."='".BLOG_URL."/download.php?url='+url_number".$num.";
				// 新增一个input
				let input".$num." = document.createElement('input') 
				// 将它隐藏（注意不能使用display或者visibility，否则粘贴不上）
				input".$num.".style.position = 'absolute' 
				input".$num.".style.zIndex = '-9'
				input".$num.".style.top = '-99%'

				document.body.appendChild(input".$num.") // 追加
				input".$num.".value = url_link".$num." // 设置文本框的内容
				input".$num.".select() // 选中文本
				document.execCommand('copy') // 执行浏览器复制命令
				copy();
	 }
			  </script>";
		$num++;	// 变量自增

	}

	echo " </tbody>";
	echo "</table>";



	echo "<center id='page'>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;共".$count_page."页&nbsp;&nbsp;&nbsp;&nbsp;";

	// 页数操作
	if($page<=1){
		echo "<a  style='color:#e20a0a' href='".$_SERVER['PHP_SELF']."?p=1'>上一页</a>";
		}else{
		echo "<a style='color:#e20a0a' href='".$_SERVER['PHP_SELF']."?p=".($page-1)."&title=".$title."'>上一页</a>";
	}
	echo "&nbsp;&nbsp;&nbsp;&nbsp;当前第&nbsp;&nbsp;".$page."&nbsp;&nbsp;页&nbsp;&nbsp;&nbsp;&nbsp;";
	if ($page<$count_page){
		echo "<a style='color:#e20a0a' href='".$_SERVER['PHP_SELF']."?p=".($page+1)."&title=".$title."'>下一页</a>";
	}else{
		echo "<a style='color:#e20a0a' href='".$_SERVER['PHP_SELF']."?p=".($count_page)."&title=".$title."'>下一页</a>";
	}
	echo "</center>";
?>
</tbody>
</table>

<!-- 弹窗提示 -->
<script>
		// 删除提示
        function del(url){
            swal(
                {title:"您确定要删除这条数据吗",
                    text:"删除后将无法恢复，请谨慎操作！",
                    type:"warning",
                    showCancelButton:true,
                    confirmButtonColor:"#DD6B55",
                    confirmButtonText:"确定删除！",
                    cancelButtonText:"取消",
                    closeOnConfirm:false,
                    closeOnCancel:false
                },
                function(isConfirm)
                {
                    if(isConfirm)
                    {
                        swal({title:"删除成功！",
                            text:"您已成功删除！",
                            type:"success",
							timer: 2000, 
							showConfirmButton: false 
							},function(){window.location="action-del.php?url="+url})
                    }
                    else{
                        swal({title:"已取消",
                            text:"您取消了删除操作！",
                            type:"error",
							timer: 1200, 
							showConfirmButton: false })
                    }
                }
            )
        }
		// 复制提示
		function copy(){
			sweetAlert({
				title:"复制成功！",
				type:"success",
				// text:"0.5秒后自动关闭",
				timer: 1200, 
				showConfirmButton: false	
				});

		}
    </script>

<div ><span id="copyright">Copyright &copy 微夏博客原创 vxia.net  </span></div>
</body>
</html>

