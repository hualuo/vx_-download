<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		后台执行修改数据
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 链接数据库
require_once("../config.php");

// 启动Session
session_start();

//  判断是否登陆
if (empty($_SESSION['username'])){
	header('location:./login.php');
}

// 接收并处理传输过来的参数
$url = htmlspecialchars($_POST["url"]);
$title = htmlspecialchars($_POST["title"]);
$ali_title =  htmlspecialchars($_POST["ali_title"]);
$ali_share = htmlspecialchars($_POST["ali_share"]);
$code = htmlspecialchars($_POST["code"]);
$other_title =  htmlspecialchars($_POST["other_title"]);
$other_share = htmlspecialchars($_POST["other_share"]);

// 执行更新数据库操作
$sql = "update vx_download_share set title='{$_POST['title']}',ali_share='{$ali_share}',code='{$code}',other_share='{$other_share}' WHERE url='{$url}'";

// 判断是否执行成功 
if($conn->query($sql))
{
  header("location:index.php");
}
else{
  echo "修改失败";
}


?>