<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		网站配置文件
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

//数据库地址
define('DB_HOST','localhost');
//数据库用户名
define('DB_USER','112233');
//数据库密码
define('DB_PASSWD','112233');
//数据库名
define('DB_NAME','112233');


// 创建连接
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

// 检测连接
if (!$conn) {
    die("连接失败: " . mysqli_connect_error());
}

// 选择数据库名 
mysqli_select_db($conn,DB_NAME);

// 编码设置
mysqli_set_charset($conn,"utf8");

// 网站系统配置
define('BLOG_NAME','微夏博客下载界面');
define('BLOG_URL','http://www.vxia.net');

// 后台管理员用户名密码 
define('USERNAME','admin');
define('PASSWORD','admin888');
define('USER_MAIL','656536055@qq.com');

